<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-07-17
 * Time: 22:23
 */
namespace harpya\cmd;

class Command {

    use CheckParameters;
    use LoadScriptFile;
    use ExecuteCommand;
    use GiveFeedback;


    public static function exec() {
        $exitCode = 0;

        // check parameters
        try {
            $actions = self::checkParameters();

            //self::addMessage(print_r($actions,true));

            // load script file
            try {
                $contents = self::loadScriptFile($actions['config']);

                //self::addMessage(print_r($contents,true));
                // execute command
                $exitCode = self::executeCommand($actions['exec'] ?? false, $contents);

            } catch (\Exception $e) {
                self::addMessage('Error: '.$e->getMessage());
                $exitCode=1;
            }

        } catch (\Exception $e) {
            self::addMessage('Error: '.$e->getMessage());
            $exitCode=1;

        }



        // give feedback
        self::giveFeedback();
        exit($exitCode);
    }





}
