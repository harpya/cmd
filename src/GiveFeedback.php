<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-07-17
 * Time: 22:35
 */

namespace harpya\cmd;


trait GiveFeedback
{
    protected static $messages = [];

    protected static function giveFeedback() {
        echo "\n\n" . join("\n", self::$messages)."\n\n";
    }

    protected static function addMessage($msg) {
        self::$messages[] = $msg;
    }

}
