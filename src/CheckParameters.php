<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-07-17
 * Time: 22:33
 */

namespace harpya\cmd;


use Aura\Cli\CliFactory;


trait CheckParameters
{

    protected static $options = [
        'help,h,?', // help
        'config,c:',    // config file
        'init,i',     // to initialize config file
    ];


    /**
     * @var \Aura\Cli\Context
     */
    protected static $context;

    protected static $actions = [];

    /**
     * @return \Aura\Cli\Context
     */
    protected static function getContext() {
        if (!self::$context) {
            $cli = new CliFactory;
            self::$context = $cli->newContext($GLOBALS);
        }
        return self::$context;
    }


    /**
     *
     */
    protected static function checkParameters() {


        $params = self::getContext()->getopt(self::$options);


        if ($params->get('-h', false) ) {
            self::$actions['help']=true;
        }

        if ($params->get('-c') || $params->get('--config')) {
            self::$actions['config'] =  ($params->get('--config') ?? HARPYA_CMD_DEFAULT_CONFIG);
        } else {
            self::$actions['config'] =  HARPYA_CMD_DEFAULT_CONFIG;
        }

        if ($params->get('--init', false) ) {
            self::initConfigFile( self::$actions['config']);
        }



        $argv = $params->get();

        $k = end(array_keys($argv));


        if (is_numeric($k) && $k>0) {
            self::$actions['exec'] = $argv[$k];
        }

        return self::$actions;

    }


    /**
     * Create a
     * @param string $configFile
     */
    protected static function initConfigFile($configFile) {

        if (file_exists($configFile)) {
            throw new \Exception("The file '$configFile' already exists.");
        }


        $values = [
            'scripts' => [
                'ls' => 'ls -la'
            ]
        ];

        file_put_contents($configFile, json_encode($values,JSON_PRETTY_PRINT));
    }
}
