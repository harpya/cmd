<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-07-17
 * Time: 22:34
 */

namespace harpya\cmd;


trait LoadScriptFile
{

    protected static function loadScriptFile($configFile) {

        if (!$configFile) {
            throw new \Exception('Config file not defined');
        }

        if (!file_exists($configFile)) {
            throw new \Exception("Config file '$configFile' does not exists");
        }

        if (!is_file($configFile)) {
            throw new \Exception("Config file '$configFile' is not a regular file");
        }



        if (!is_readable($configFile)) {
            throw new \Exception("Config file '$configFile' is not readable");
        }

        $contents = file_get_contents($configFile);
        $json = json_decode($contents,true);
        return $json;
    }

}
