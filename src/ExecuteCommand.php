<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-07-17
 * Time: 22:35
 */

namespace harpya\cmd;

use mikehaertl\shellcommand\Command;

trait ExecuteCommand
{


    protected static function executeCommand($exec=false, $commands=[]) {

        if (!$exec) {
            throw new \Exception('Exec command not informed');

        }

        if (!is_array($commands) || !isset($commands['scripts']) ) {
            throw new \Exception("Invalid config contents");
        }

        if (!isset($commands['scripts'][$exec])) {
            throw new \Exception("Command $exec does not exists in config contents");
        }
        $cmd = $commands['scripts'][$exec];


        self::addMessage("Executing the $exec command ( ".$commands['scripts'][$exec]." )" );

        $command = new Command($cmd);
        if ($command->execute()) {
            $output = $command->getOutput();
            self::addMessage("\nOutput:\n".print_r($output,true)."\n");
            $exitCode = 0;
        } else {
            $error = $command->getError();;
            self::addMessage("\nError:\n".print_r($error,true)."\n");
            $exitCode = $command->getExitCode();
        }
        return $exitCode;
    }
}
